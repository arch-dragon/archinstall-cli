import json
import os
import re

from collections import defaultdict

MOUNTING_POINT="/mnt/archinstall"

def command(command, dryRun):
    if dryRun:
        print(command)
    else:
        if os.system(command) != 0:
            print (f"command '{command}' failed")
            quit()

def chroot(bash, dryRun):
    chrootu("root",bash,dryRun)
    # command(f"arch-chroot /mnt {bash}", dryRun)

def chrootu(user,bash,dryRun):
    command(f"arch-chroot -u {user} {MOUNTING_POINT} {bash}", dryRun)

def chinstall(packages,dryRun):
    chinstallu("root", "pacman", packages, dryRun)
    # chroot(f"pacman -S --needed --noconfirm {packages}", dryRun)

def chinstallu(user,helper,packages,dryRun):
    chrootu(user,f"bash -c 'HOME=/workdir/home && {helper} -S --needed --noconfirm {packages}'", dryRun)

def merge_dict(d1, d2):
    dd = defaultdict(list)

    for d in (d1, d2):
        for key, value in d.items():
            dd[key] = value
    return dict(dd)



def applyArgs(options, args):
    if not args.keyboard is None:  # todo: check validity
        options["keyboard"] = args.keyboard

    if not args.language is None:  # todo: check validity
        options["language"] = args.language

    if not args.timezone is None:  # todo: check validity
        options["timezone"] = args.timezone

    if not args.encoding is None:  # todo: check validity
        options["encoding"] = args.encoding

    if not args.hostname is None:
        options["hostname"] = args.hostname

    if not args.rootPassword is None:
        options["!root-password"] = args.rootPassword

    if not args.shell is None:
        options["shell"] = args.shell

    if (not "aur-helper" in options) or (not args.aurHelper is None):
        options["aur-helper"] = args.aurHelper

    return options


def askYesNo(message):
    response = ""
    while(response.lower() != "y" and response.lower() != "n"):
        response = input(message)
    return response.lower()

def prepareProfile(profile,dryRun):
    thisFolderPath = os.path.dirname(os.path.realpath(__file__))
    profilesFolder = f"{thisFolderPath}/../profiles"
    folder=""

    if ("http:" in profile) or ("https:" in profile):
        if ".git" in profile:
            p = re.compile(r"\/([a-zA-Z0-9\-]+)\.git")  
            folder=f"{profilesFolder}/{p.findall(profile)[0]}"

            command(f"rm -rf {folder}",dryRun)
            command(f"cd {profilesFolder} && git clone {profile}",dryRun)
    else:
        folder = f"{profilesFolder}/{profile}"

    return folder

#-------AUR HELPERS-------

def install_aur_helper(package,superuser,dryRun):
    if package == "paru":
        install_paru(superuser,dryRun)
    else:
        print(f"installation of {package} is yet not supported")

def install_paru(superuser, dryRun):
    chinstall("base-devel git",dryRun)
    chroot("mkdir /workdir/home", dryRun)
    chroot("chmod 777 /workdir/home", dryRun)
    chrootu(superuser,"bash -c 'cd /workdir && git clone https://aur.archlinux.org/paru.git'",dryRun)
    chrootu(superuser,"bash -c 'HOME=/workdir/home && cd /workdir/paru && makepkg -fsic --noconfirm'",dryRun)

#--------------

def configure_pacman(config,dryRun):
    if config.get("options"):
        options=config["options"]
        for option in options:
            if option == "ILoveCandy": 
                print(f"{option}: not supported by pace")
                continue
            if (f"{options[option]}" == "True"):
                chroot(f"pace set {option}", dryRun)
            elif (f"{options[option]}" == "False"):
                chroot(f"pace unset {option}", dryRun)
            else:
                chroot(f"pace set {option} {options[option]}", dryRun)
    
    if config.get("repositories"):
        for repo in config["repositories"]:
            if repo.get("key"):
                if repo.get("keyserver"):
                    chroot(f"pacman-key --recv-key '{repo['key']}' --keyserver '{repo['keyserver']}'",dryRun)
                else:
                    chroot(f"pacman-key --recv-key '{repo['key']}'",dryRun)
            if repo.get("packages"):
                chroot(f"pacman -Sy --noconfirm --needed wget",dryRun)
                chroot(f"mkdir -p /workidr/tmp",dryRun)
                for package in repo["packages"]:
                    chroot(f"bash -c 'cd /workidr/tmp && wget {package}'",dryRun)
                chroot(f"bash -c 'cd /workidr/tmp && pacman -U --noconfirm $(ls)'",dryRun)
                
            arguments=""
            if repo.get("include"):
                arguments+=f" --include '{repo['include']}'"
            if repo.get("server"):
                arguments+=f" --server '{repo['server']}'"
            if repo.get("siglevel"):
                for siglevel in repo["siglevel"]:
                    arguments+=f" --siglevel '{siglevel}'"

            chroot(f"pace add {repo['name']} {arguments}",dryRun)
            chroot(f"pacman -Sy",dryRun)

def install_packages(packages,superuser,helper,dryRun):
    toinstall=""
    for group in packages:
        if group != "aur":
            toinstall+=" "+packages[group]
    chroot("pacman -Sy", dryRun)
    chinstall(toinstall, dryRun)
    if packages.get("aur"):
        chinstallu(superuser, helper, packages["aur"], dryRun)

def configure_shell(shell,users,dryRun):
    chinstall(shell, dryRun)
    for user in users:
        chroot(f"chsh -s /bin/{shell} {user}",dryRun)

def enable_services(services,dryRun):
    for service in services:
        chroot(f"systemctl enable {service}",dryRun)

def install_flatpaks(packages,user,dryRun):
    chinstall("flatpak", dryRun)
    chroot(f"flatpak install --noninteractive {packages}",dryRun)
    
def execute_post_install_scripts(postInstallFolder,dryRun):
    fileList = []
    for root, dirs, files in os.walk(postInstallFolder):
        for file in files:
            fileList.append(os.path.join("post-install", file))

    command(f"cp -r {postInstallFolder} {MOUNTING_POINT}/workdir/", dryRun)

    fileList.sort()
    for path in fileList:
        print(f"Executing: {path}")
        chroot(f"bash '/workdir/{path}'", dryRun)