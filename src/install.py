import argparse
import sys
import os
import archinstall
import json

from functions import command
from functions import prepareProfile
from functions import chroot
from functions import chrootu
from functions import chinstall
from functions import chinstallu
from functions import install_aur_helper
from functions import configure_pacman
from functions import install_packages
from functions import install_flatpaks
from functions import configure_shell
from functions import enable_services
from functions import execute_post_install_scripts

parser = argparse.ArgumentParser()

parser.add_argument("-p", "--profile", dest="profile",
                    default="default-kde", help="Sets the profile to use")
# parser.add_argument("-r", "--root-password", dest="rootPassword",
#                     default=None, help="Overrides the root password")

parser.add_argument("-u","--username",dest="username",default=None, help="Overrides or adds the first username")
#parser.add_argument("-up","--user-password",dest="userPassword",default=None, help="Overrides or adds the first user password")
parser.add_argument("--hostname", dest="hostname",

                    default=None, help="Overrides the hostname")
parser.add_argument("-t", "--timezone", dest="timezone",
                    default=None, help="Overrides the timezone")
parser.add_argument("-k", "--keyboard", dest="keyboard",
                    default=None, help="Overrides the keyboard layout")
parser.add_argument("-l", "--language", dest="language",
                    default=None, help="Overrides the installed language")
parser.add_argument("-e", "--encoding", dest="encoding",
                    default=None, help="Overrides the locale encoding")
# parser.add_argument("-s", "--shell", dest="shell",
#                     default=None, help="Overrides installed shell")
parser.add_argument("-d", "--dry-run", dest="dryRun", default=False,
                    help="Prints the commands without executing them", action="store_true")
# parser.add_argument("--aur-helper", dest="aurHelper",
#                     default=None, help="Overrides the aur helper")

args = parser.parse_known_args()[0]

dryRun=args.dryRun

forbidden = []
forbidden.append("-p")
forbidden.append("--profile")

arguments = ""
skipnext=False
for arg in sys.argv:
    if skipnext:
        skipnext=False
        continue
    if not arg.find("wrapper.py") >= 0:
        if not forbidden.count(arg) > 0:
            arguments+=f" {arg}"
        else:
            skipnext=True

print(sys.argv)
print(arguments)

if( os.path.isdir("/tmp/archdragon")):
    print("Removing old archdragon tmp files")
    command("rm -r /tmp/archdragon", dryRun)

command("mkdir -p /tmp/archdragon/profile", dryRun)

profilePath = prepareProfile(args.profile, dryRun)

if(not os.path.isdir(profilePath)):
    print(f"ERROR: profile at {profilePath} does not exists")
    quit(1)

command(f"cp -r {profilePath}/* /tmp/archdragon/profile", dryRun)
profilePath="/tmp/archdragon/profile"

userConfigPath=f"{profilePath}/archinstall/user_configuration.json"
credConfigPath=f"{profilePath}/archinstall/user_credentials.json"
diskConfigPath=f"{profilePath}/archinstall/user_disk_layout.json"

quitMissingConfigs=False
if(not os.path.isfile(userConfigPath)):
    print(f"ERROR: profile '{args.profile}' is missing archinstall configuration")
    quitMissingConfigs=True
if(not os.path.isfile(credConfigPath)):
    print(f"ERROR: profile '{args.profile}' is missing archinstall credentials configuration")
    quitMissingConfigs=True
if(not os.path.isfile(diskConfigPath)):
    print(f"ERROR: profile '{args.profile}' is missing archinstall disk layout configuration")
    quitMissingConfigs=True
if(quitMissingConfigs):
    quit(2)

arguments+=f" --config {userConfigPath}"
arguments+=f" --creds {credConfigPath}"
arguments+=f" --disk-layout {diskConfigPath}"

print("Read configuration files")
userConfigFile=open(userConfigPath,"r")
userConfigJson=json.loads(userConfigFile.read())
userConfigFile.close()

diskConfigFile=open(diskConfigPath,"r")
diskConfigJson=json.loads(diskConfigFile.read())
diskConfigFile.close()

credConfigFile=open(credConfigPath,"r")
credConfigJson=json.loads(credConfigFile.read())
credConfigFile.close()

print("Prepare profile changes")

harddrive = archinstall.select_disk(archinstall.all_blockdevices(partitions=False)).path

userConfigJson["harddrives"]=[f"{harddrive}"]
if not args.encoding is None:
    userConfigJson["sys-encoding"]=args.encoding
if not args.timezone is None:
    userConfigJson["timezone"]=args.timezone
if not args.language is None:
    userConfigJson["sys-language"]=args.language
if not args.keyboard is None:
    userConfigJson["keyboard-layout"]=args.keyboard
if not args.hostname is None:
    userConfigJson["hostname"]=args.hostname

diskConfigJson[harddrive]=diskConfigJson["/device"]
del diskConfigJson["/device"]

userConfigFile=open(userConfigPath,"w")
userConfigFile.write(json.dumps(userConfigJson))
userConfigFile.close()

diskConfigFile=open(diskConfigPath,"w")
diskConfigFile.write(json.dumps(diskConfigJson))
diskConfigFile.close()



# "disk-format": "/tmp/archdragon/profile/archinstall/user_disk_layout.json",
# "harddrives": [],

print("Update keyring")
command("pacman -Sy --noconfirm --needed archlinux-keyring", dryRun)

print("Launching archinstall")
test="cd ./src && "
command("mkdir -p ./src/profiles", False)
command(f"{test}cp archdragon.py ./profiles/archdragon.py",False)
command(f"{test}archinstall --dry-run --script archdragon {arguments}", False)
#command(f"archinstall --dry-run {arguments}", False)


# Install additional configs

additionalConfigPath=f"{profilePath}/config.json"

if(not os.path.isfile(additionalConfigPath)):
    quit(0)

chroot("mkdir /workdir",dryRun)
chroot("chmod 777 /workdir",dryRun)

print("Processing additional configs")

additionalConfigFile=open(additionalConfigPath,"r")
config=json.loads(additionalConfigFile.read())
additionalConfigFile.close()

superuser="default"
users=["root"]
for user in credConfigJson["!users"]:
    users.append(user["username"])
    if user["sudo"]:
        superuser=user["username"]


command("echo \"%wheel ALL=(ALL:ALL) NOPASSWD: ALL\" >> /mnt/archinstall/etc/sudoers.d/tmp",dryRun)

if config.get("aur-helper"):
    install_aur_helper(config["aur-helper"], superuser, dryRun)

if config.get("pacman"):
    chinstallu(superuser,config["aur-helper"],"pace",dryRun)
    configure_pacman(config["pacman"], dryRun)

if config.get("packages"):
    install_packages(config["packages"],superuser,"paru",dryRun)

command("rm /mnt/archinstall/etc/sudoers.d/tmp",dryRun)


if config.get("flatpaks"):
    install_flatpaks(config["flatpaks"],superuser,dryRun)

if config.get("shell"):
    configure_shell(config["shell"],users,dryRun)

if config.get("services"):
    enable_services(config["services"],dryRun)

# if config.get("chezmoi"):
#    chinstall("chezmoi", dryRun)
#    chrootu(config["chezmoi"]["user"], f"chezmoi init {config['chezmoi']['repository']}", dryRun)
#    chrootu(config["chezmoi"]["user"],"chezmoi apply -v")

postInstallFolder = f"{profilePath}/post-install"
if os.path.isdir(postInstallFolder):
    print("Executing post install scripts")
    execute_post_install_scripts(postInstallFolder,dryRun)