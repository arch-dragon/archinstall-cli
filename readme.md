
## About

The goal of this project is to make a simple and customizable cli arch installer with minimal user interaction.

It's my first time writing in python so the code may not be the best out there.

## How To

First you need to flash a working arch iso. You can either build one yourself with [this](https://gitlab.com/arch-dragon/archinstall-cli-iso) or use the [official](https://archlinux.org/download/) one.
Keep in mind that the official one my lack some requirements. (currently only git)

### Using the custom iso
1. simply type "auto-install" and the installation will start

### Using the official iso
1. install git
1. clone this repository in the $home folder
1. checkout to the lastest tag
    > git checkout $(git describe --tags --abbrev=0)
1. run this:
    > python archinstall-cli/src/install.py

### Options
     

|   |   |   
| --- | --- |
| -p, --profile | Sets the profile to install (can be either the name of a  profile included in this repository or a git repository) |
| -rp, --root-password | Overrides or sets the root password |
| --hostname | Overrides the hostname |
| -t, --timezone | Overrides the timezone |
| -k, --keyboard | Overrides the keyboard layout (may not be picked up by some DEs) |
| -l, --language | Overrides the installed language |
| -e, --encoding | Overrides the locale encoding |
| -s, --shell | Overrides the installed shell |
| -d, --dryrun | Prints the commands without executing them |
| --aur-helper | Overrides the aur helper  (for now only paru is supported) |
|   |   |


## TODO 
- Add support for custom system configuration files
- Add better support for partition handling
- Add support for installing dotfiles
- Add support for overrinding packages
- Add support for non super user

## Contributions

Altought I'll only fully support [my configuration](https://gitlab.com/Snogard/arch-profile) and "default-kde", you are welcome to submit issues and merge requests.  

If you submit a merge request, please do so in a new branch.

Popular user created profiles may be added to the repository.
